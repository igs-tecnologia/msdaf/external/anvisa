FROM msdaf/nginx-alpine-python

WORKDIR /usr/share/nginx/service

COPY service/ .

RUN pip3 install -r requirements.txt
ENTRYPOINT flask run --host=0.0.0.0
